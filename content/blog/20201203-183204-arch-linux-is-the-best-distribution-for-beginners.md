---
title: Arch Linux is the best distribution for beginners
date: 2020-12-03T18:02:36.201Z
path: blog/2020-12-3-arch-linux-best-distribution-beginners
tags:
  - linux
---
This a stub/draft for now, public though. 

 
* AUR is the best for beginners
* `yay` pacman wrapper is amazing, it gets from official Arch repo AND AUR. 
* Install 99% of packages this  if doesn't exist can make your own AUR, but most of the time someone has already done this. 
* AUR downfalls:
  * Most are not public repos to submit merge/pull requests against. I think they should all be on an AUR GitLab for collaboration
  * Only way to communicate is to in comments on the AUR package page
* Other distros are a nightmare for installing things
