---
title: Netlify CMS is setup (#ultraworking)
date: 2020-05-24T23:24:57.781Z
path: /blog/netlify-cms-setup-ultraworking
tags:
  - netlify
  - gatsbyjs
  - ultraworking
---
I am writing this from elijahlynn.net/admin which is using the Netlify CMS and easily lets me add images and just have a better UI to enter content. The idea here is that it reduces friction and then I can actually write more!\
\
Testing a screenshot upload right here. Hrm, cannot paste images, need to upstream that. I did add the image below from the UI though, which is great. 

![](../assets/2020-05-24_16-25.png)

I started this blog in the beginning of 2019 doing an [Ultraworking](https://www.ultraworking.com/) marathon and didn't really do much since then. Now I just did another Ultraworking session now that I signed up for 24/7 workcycles with [The Work Gym](https://www.ultraworking.com/twg) and finally got this last step finished in a focused 5.5 hour work session! This is fantastic! Ultraworking is sooo effective.