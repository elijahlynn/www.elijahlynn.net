---
title: Writing just to write
date: 2020-06-19T17:20:44.888Z
path: /blog/2020-06-19-writing-just-to-write
tags:
  - life
---
Writing just to write. Writing is a habit that I want to create in my life. Something about the loop of outputting, then reading it back and inputting it back into my brain and the cycle of that all that really helps me think better.

Part of the habit is just writing, and I haven't yet written here since I got the Netlify CMS functionality added to this site therefore I haven't actually gotten to use this feature, which I am doing right now to write this post. \
\
I love the real-time preview that happens on the right side pane!

![Animated GIF showing the realtime preview of Netlify CMS](../assets/netlify-cms-preview.gif "Netlify CMS Real-time Preview")

And another thing I really like is that Netlify CMS is 100% open source (as opposed to Medium, which is closed source). This means that I can actually tweak and modify the functionality as I like and when I find a bug I can file an upstream issue here <https://github.com/netlify/netlify-cms>. \
\
Okay, time to publish, all about the momentum!