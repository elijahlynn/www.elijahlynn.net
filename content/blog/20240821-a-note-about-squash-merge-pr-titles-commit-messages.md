---
title: A note about Merge/Pull Request titles and commit messages
date: '2024-08-21'
path: "/blog/2024-08-21-a-note-about-squash-merge-pr-titles-commit-messages"
tags: ["git", "github"]
---

Merge Request (GitLab) and Pull Request (GitHub) titles are commit messages when using a squash and merge strategy on GitHub or GitLab. Write your titles like they are going to be a commit message, because they are. 

examples of not great commit messages:
* Fixed stuff (fixed what?)
* add initial config (for what?)
* WIP
* lol forgot this

examples of better commit messages:
* Fix null pointer exception in UserService when user data is null
* Improve load times by optimizing image assets
* Remove deprecated API usage from OrderService
* Update dependencies to latest versions to fix security vulnerabilities

