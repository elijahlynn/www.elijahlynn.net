---
title: Ultra Working Plus Run Breaks
date: '2019-01-08'
path: "/blog/2019-01-08-ultra-working-plus-run-breaks"
tags: ["ultraworking", "running"]
---

tldr; Ultra Working + Run Breaks resulted in me running nearly 2 miles in short 2-3 minute increments.  

Over this past weekend I completed an Ultra Working "[Work Marathon](http://www.workmarathon.com)" which was a great success. The result of it was that I built this blog and now have a place to publish this article, which is something I had put off for quite sometime, yay for progress here!

Yesterday was the first day back at my day job, where I doing DevOps and Drupal on VA.gov. Around noon, I decided to go ahead and apply the same format to yesterday which was a fantastic success. The work cycles are 30 minutes each with a 10 minute break in between. I was able to get 9 work cycles in from 12:40pm to 6:30pm.

Now, instead of just taking the 10 minute break I modified them with something I have been doing off and on for the past 1-2 years. Which is do short, fast "Run Breaks". A Run Break is almost just like a cigarette break (yes, I used to smoke the nicotine when I was a teenager) but obviously leaves you in a better state. I love Run Breaks because they don't require a large mental effort or even changing into running clothes. Before, if I had made a goal to run a mile, I might have easily put it off whereas with a Run Break, it is so short that is hard to fail. 

![The soles of my Altra Lonepeak 3.0 running shoes, heavily worn down](../assets/soles-altra-lone-peak-3.0-running-shoes-run-breaks.jpg)
 > The wear on my Altra running shoes of all my run breaks over the past 1-2 years

Before Ultra Working though, I was lucky to get 1-2 Run Breaks in per day, and some days and weeks I would skip them altogether. I always had a goal of getting to do 1 Run Break per hour, especially since I work at home and know there are health consequences to those who don't exercise and remain sedentary. So us "work at homers" need to be proactive about activating our lymph system and moving some blood around from time to time. So, I had a vision of running more and then came this past weekends Work Marathon. 
 
So back to yesterday, I applied the Work Marathon format and made a copy of the Google Sheet that was provided to us, this sheet is key to the program and gives the structure and sets the cadence. I also combined it with my lifetime subscription to [Focus@Will](http://ssqt.co/mQd5Zg9) (referral link, $20 credit for you, $20 gift card to me) and some Bose QC35 Noise Cancelling headphones which together are like a peanut butter and jelly sandwich, perfect pairing! Then for the breaks I decided to do combine my Run Breaks during the 10 minute break. I was amazed, it worked! Over the next 7-8 cycles, I ran around the block every break! Each run around the block is a little over .25 miles which meant I had run close to 2 miles! And the best thing about it is that not only was the Work Cycle format successful during my day job but I was able to incorporate my goal of running every hour into it. 

![Google Map distance measurement of my Run Break around the block](../assets/my-run-around-the-block-quarter-mile.png)
> Google Map distance measurement of my Run Break around the block

In closing, the Work Cycles format combined with the noise cancelling headphones and Focus@Will were already the perfect match, but adding in Run Breaks really tops it off!
