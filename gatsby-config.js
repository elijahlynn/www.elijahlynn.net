module.exports = {
  siteMetadata: {
    title: `ElijahLynn.net`,
    author: `Elijah Lynn`,
    description: `Blog of Elijah Lynn: Technology, Life Lessons, Music, Portland`,
    siteUrl: `https://www.elijahlynn.net`,
    social: {
      twitter: `elijahlynn`,
    },
  },


  plugins: [
    'gatsby-redirect-from',
    'gatsby-plugin-meta-redirect', // make sure this is always the last one
    {
      resolve: `gatsby-plugin-netlify-cms`,
      options: {
        enableIdentityWidget: false,
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/blog`,
        name: `blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/assets`,
        name: `assets`,
      },
    },
    {
      // See options at https://github.com/Creatiwity/gatsby-plugin-favicon
      resolve: `gatsby-plugin-favicon`,
      options: {
        logo: "./src/favicon.png"
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 590,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          `gatsby-remark-prismjs`,
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`,
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-sitemap`,
  {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-32695586-1`,
      },
    },
    `gatsby-plugin-feed`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `ElijahLynn.net`,
        short_name: `ElijahLynn.net`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `red`,
        display: `browser`,
        icon: `src/favicon.png`,
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
  ],
}
